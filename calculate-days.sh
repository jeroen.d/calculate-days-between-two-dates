#!/bin/bash
# days-between.sh:    Number of days between two dates.
# Usage: ./calculate-days DD-MM-YYYY DD-MM-YYYY
# Or create alias: alias daysbetween='~/scripts/calculate-days-between-dates/calculate-days'

# Script based on: http://www.codelibary.com/snippet/765/number-of-days-between-two-dates

E_PARAM_ERR=65        # Param error.

REFYR=1600            # Reference year.
CENTURY=100
DIY=365
ADJ_DIY=367           # Adjusted for leap year + fraction.
MIY=12
DIM=31
LEAPCYCLE=4

MAXRETVAL=255         #  Largest permissable
                      #+ positive return value from a function.

diff=                 # Declare global variable for date difference.
value=                # Declare global variable for absolute value.
day=                  # Declare globals for day, month, year.
month=
year=


Param_Error ()        # Command line parameters wrong.
{
  # echo "Usage: `basename $0` [D]D/[M]M/YYYY [D]D/[M]M/YYYY"
  echo calcdate DD-MM-YYYY DD-MM-YYYY
  echo "       (enter at least 1 date)"
  exit $E_PARAM_ERR
}


Parse_Date ()                 # Parse date from command line params.
{
  day=${1%%-**}
  dm=${1%-**}                 # Day and month.
  month=${dm#*-}
  year=${1:(-4)}
  # let "year = `basename $1`"  # Not a filename, but works just the same.
}


check_date ()                 # Checks for invalid date(s) passed.
{
  [ "$day" -gt "$DIM" ] || [ "$month" -gt "$MIY" ] || [ "$year" -lt "$REFYR" ] && Param_Error
  # Exit script on bad value(s).
  # Uses "or-list / and-list".
  #
  # Exercise: Implement more rigorous date checking.
}


strip_leading_zero () #  Better to strip possible leading zero(s)
{                     #+ from day and/or month
  return ${1#0}       #+ since otherwise Bash will interpret them
}                     #+ as octal values (POSIX.2, sect 2.9.2.1).


day_index ()          # Gauss' Formula:
{                     # Days from Jan. 3, 1600 to date passed as param.

  day=$1
  month=$2
  year=$3

  let "month = $month - 2"
  if [ "$month" -le 0 ]
  then
    let "month += 12"
    let "year -= 1"
  fi

  let "year -= $REFYR"
  let "indexyr = $year / $CENTURY"


  let "Days = $DIY*$year + $year/$LEAPCYCLE - $indexyr + $indexyr/$LEAPCYCLE + $ADJ_DIY*$month/$MIY + $day - $DIM"
  #  For an in-depth explanation of this algorithm, see
  #+ http://home.t-online.de/home/berndt.schwerdtfeger/cal.htm


  echo $Days

}


calculate_difference ()            # Difference between to day indices.
{
  let "diff = $1 - $2"             # Global variable.
}


abs ()                             #  Absolute value
{                                  #  Uses global "value" variable.
  if [ "$1" -lt 0 ]                #  If negative
  then                             #+ then
    let "value = 0 - $1"           #+ change sign,
  else                             #+ else
    let "value = $1"               #+ leave it alone.
  fi
}


if [ $# -lt "1" ] || [ $# -gt "2" ]  # Require one or two command line params.
then
  Param_Error
fi

if [ $# -lt "2" ]
then
  date1=$(date +"%d-%m-%Y")
  date2=$1
else
  date1=$1
  date2=$2
fi


Parse_Date $date1
check_date $day $month $year       #  See if valid date.

strip_leading_zero $day            #  Remove any leading zeroes
day=$?                             #+ on day and/or month.
strip_leading_zero $month
month=$?

let "date1 = `day_index $day $month $year`"


Parse_Date $date2
check_date $day $month $year

strip_leading_zero $day
day=$?
strip_leading_zero $month
month=$?

date2=$(day_index $day $month $year) # Command substitution.


calculate_difference $date1 $date2

abs $diff                            # Make sure it's positive.
diff_days=$value

diff_weeks=$(echo "scale = 1; $diff_days / 7" | bc -l)
diff_years=$(echo "scale = 2; $diff_days / $DIY" | bc -l)
echo $diff_days dagen
echo $diff_weeks weken

if [ "${diff_years:0:1}" == "." ]; then
  diff_years="0${diff_years}"          # Add extra zero
fi
echo ${diff_years} jaar

exit 0
