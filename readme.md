**Make the file exacutable:**
`chmod +x calculate_days.sh`

**Run the script:**
`./calculate_days.sh 01-01-2020 01-01-2021`

Or use just one parameter to use the current date
`./calculate_days.sh 01-01-2021`

**Pro tip:**
Create an alias for ./calculate_days.sh in your .bash_profile or .zshrc file!
`alias daysbetween='~/scripts/calculate-days-between-two-dates/calculate-days.sh'`
